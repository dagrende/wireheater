/*
  Thermostat for heated wire.
  Regulate wire temp with the voltage 0-5V on A3. Use a potentiometer to control the voltage.
  The current through the wire is controlled by a FET transistor turning the voltage on/off 
  by thefetOnOutputPin. Temp is measured by measuring voltage over and current through the 
  heated wire. Wire resistance is proportional to wire temp. A PID regulator monitors the 
  wire resistance and adjusts the PWM duty cycle on on FET on output pin, to keep the temp constant.
*/

#include <Arduino.h>
#include <LiquidCrystal_I2C.h>

#define UL unsigned long

const int currentInputPin = A0;
const int voltageInputPin = A1;
const int tempKnobInputPin = A2;
const int fetOnOutputPin = 9;

const float tempKnobScale = 1.0 / 1023; // from 0 to 1
const float voltageScale = 12.5/458;  // voltage divider 2.2K+10K with max input voltage 27V
const float currentScale = 3.636 / 96; // 3.636A is 96 raw - measured with 3.3ohm at 12V
const float fetOnScale = 255.0;

UL nextCsvPrintTime = 0;
boolean printCsv = false;
boolean saveCold = false;

LiquidCrystal_I2C lcd = LiquidCrystal_I2C(0x27, 20, 4);

// command line input stuff
const int recBufLen = 100;
char recBuf[recBufLen] = {0};
int recBufIndex = 0;
boolean recLineReceived = false;

// put all received characters in recBuf and set recLineReceived true when a CR is received
void pollSerial() {
  while (Serial.available()) {
    char ch = Serial.read();
    if (recBufIndex < recBufLen && !recLineReceived) {
      if (ch == 13) {
        ch = 0;
        recLineReceived = true;
      }
      recBuf[recBufIndex++] = ch;
    }
  }
}

void resetRecLine() {
  recBufIndex = 0;
  recLineReceived = false;
}


// printf to an lcd row
template <typename... T>
void lcdprintf(int row, const char *fmt, T... args) {
  int len = snprintf(NULL, 0, fmt, args...);
  if (len) {
    char buff[len+1];
    sprintf(buff, fmt, args...);
    buff[len] = 0;
    lcd.setCursor(0, row);
    lcd.print(buff);
  }
}


struct {
  float p, i, d;
} pidParams = {6.0, 0.4, 0};
UL prevTime;
float prevError;

void printPidConfig() {
    Serial.print("pid_config{");
    Serial.print(pidParams.p, 5);
    Serial.print(", ");
    Serial.print(pidParams.i, 5);
    Serial.print(", ");
    Serial.print(pidParams.d, 5);
    Serial.println("}");
}

int target = 0;

void executeCommand(char *line) {
  Serial.print("executeCommand("); Serial.print(line); Serial.println(")");
  char s[10];
  if (1 == sscanf(recBuf, "p=%s", s)) {
    pidParams.p = atof(s);
    printPidConfig();
  } else if (1 == sscanf(recBuf, "i=%s", s)) {
    pidParams.i = atof(s);
    printPidConfig();
  } else if (1 == sscanf(recBuf, "d=%s", s)) {
    pidParams.d = atof(s);
    printPidConfig();
  } else if (1 == sscanf(recBuf, "t=%d", &target)) {
    Serial.print("target="); Serial.println(target);
  } else if (strcmp(recBuf, "s")) {
    saveCold = true;
  }
}

UL nextColdCurrentMeasureTime = 0;
UL nextPidStepTime = 0;

void setup() {
  pinMode(fetOnOutputPin, OUTPUT);
  Serial.begin(115200);
  lcd.init();
  lcd.backlight();

  recBuf[0] = 0;
  
  prevTime = millis();

}

float coldVoltage = 0.0;
float coldCurrent = 0.0;
boolean headerPrinted = false;
float integral = 0.0;

void loop() {
  float tempKnob = 0.0;
  float voltage = 0.0;
  float current = 0.0;
  float error = 0.0;
  float output = 0.0;
  float derivative = 0.0;
  float rc = 0.0;
  float r = 0.0;
  float rt = 0.0;
  float ic = 0.0;
  int powerRaw = 0;
  UL now = millis();

  if (now > nextPidStepTime) {
    tempKnob = analogRead(tempKnobInputPin) * tempKnobScale;
    if (tempKnob < 0.01) {
      tempKnob = target / 100.0;
    }

    // measure voltage and current as fast as possible
    digitalWrite(fetOnOutputPin, 1);  // full power
    delayMicroseconds(10);  // let voltage and current settle
    int voltageRaw = analogRead(voltageInputPin);
    int currentRaw = analogRead(currentInputPin);
    analogWrite(fetOnOutputPin, powerRaw);  // set current power

    voltage = voltageRaw * voltageScale;
    current = currentRaw * currentScale;

    if (tempKnob < 0.01) {
      if (saveCold) {
        saveCold = false;
        coldVoltage = voltage;
        coldCurrent = current;
        Serial.println("saved cold");
      }
      powerRaw = 0;
      integral = 0.0;
      derivative = 0.0;
      prevError = 0.0;
      error = 0.0;
    }
    if (coldCurrent > 0) {
      rc = coldVoltage / coldCurrent;
    }

    // resistance
    if (current > 0) {
      r = voltage / current;
    }

    // target resistance
    rt = rc * (1 + 2.0 * tempKnob);
    
    // PID step
    UL stepTime;
    if (tempKnob > 0.01 && coldCurrent > 0 && current > 0) {
      UL stepStart = millis();

      // get PID output
      error = rt - r;
      float dt = (now - prevTime) / 1000.0;
      integral = integral + error * dt;
      derivative = (error - prevError) / dt;
      output = pidParams.p * error + pidParams.i * integral + pidParams.d * derivative;
      prevError = error;
      
      // set heating power
      long power = output * fetOnScale;
      analogWrite(fetOnOutputPin, min(255, max(0, power)));

      stepTime = millis() - stepStart;
    }
    prevTime = now;
    nextPidStepTime = now + 100;
  }
  if (now > nextCsvPrintTime) {
    // if (printCsv) {
    //   if (!headerPrinted) {
    //     Serial.println("\"rt\",\"r\"");
    //     headerPrinted = true;
    //   }
      Serial.print(rt); Serial.print(","); Serial.print(r); Serial.print(","); Serial.println(output);
    // } else {
    //   headerPrinted = false;
    // }
    
    // lcd.clear();
    // lcdprintf(0, "%.1fV %.1fA %.3f\xf4", coldVoltage, coldCurrent, rc);
    // lcdprintf(1, "%.1fV %.1fA %.3f\xf4", voltage, current, r);
    // lcdprintf(2, "kn %.1f t %.1f\xf4", tempKnob, rt);
    // lcdprintf(3, "e %.3f\xf4  o %.1f", error, output);
    nextCsvPrintTime = now + 100;
  }

  pollSerial();
  if (recLineReceived) {
    executeCommand(recBuf);
    resetRecLine();
  }
}